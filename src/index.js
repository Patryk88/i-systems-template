import '../src/styles/index.scss';

const hamburger = document.getElementById('hamburger__toggle');
const btnEl = document.querySelector('.header__shoppingButton')
const carEl = document.querySelector('.header__shoppingCart')
const usrEl = document.querySelector('.header__userInfo')
const searchEl = document.querySelector('.header__searchBox');

hamburger.addEventListener('click', () => {
  let div = document.createElement('div');
  div.className = 'header__menu';
  let menu = document.querySelector('.header__menu');

  if (!menu) {
    btnEl.classList.add('display__items');
    carEl.classList.add('display__items');
    usrEl.classList.add('display__items');
    searchEl.classList.add('display__items');
    document.querySelector('.main__container').appendChild(div);
  } else {
    document.querySelector('.header__menu').remove();
    btnEl.classList.remove('display__items');
    carEl.classList.remove('display__items');
    usrEl.classList.remove('display__items');
    searchEl.classList.remove('display__items');
  }
})